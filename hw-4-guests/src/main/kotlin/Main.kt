import java.util.*
import java.io.File

fun main2(nSize: Int, mSize: Int, nP: Int, mP: Int) : Array<Array<Int>> {
    val nPlace = nP - 1
    val mPlace = mP - 1
    val arr2d = Array(nSize) {Array(mSize) {404}}
    var cnt = nSize*mSize
    arr2d[nPlace][mPlace] = cnt
    cnt--
    var nStart = nPlace
    val mStart = mPlace
    var n : Int
    var m : Int
    while (cnt > 0) {
        nStart--
        if (nStart >= 0) {
            arr2d[nStart][mStart] = cnt
            cnt--
        }
        n = nStart
        m = mStart
        while(n != nPlace){
            n++
            m--
            if ((n>=0 && m>=0) && (n<nSize && m<mSize)) {
                arr2d[n][m] = cnt
                cnt--
            }
        }
        while(m != mPlace){
            n++
            m++
            if ((n>=0 && m>=0) && (n<nSize && m<mSize)) {
                arr2d[n][m] = cnt
                cnt--
            }
        }
        while(n != nPlace){
            n--
            m++
            if ((n>=0 && m>=0) && (n<nSize && m<mSize)) {
                arr2d[n][m] = cnt
                cnt--
            }
        }
        while(m != mPlace+1){
            n--
            m--
            if ((n>=0 && m>=0) && (n<nSize && m<mSize)) {
                arr2d[n][m] = cnt
                cnt--
            }
        }
    }
    return arr2d
}

fun main(args: Array<String>) {
    //print("Введите n и m размеры зала: ")
    //var (nSize, mSize) = readLine()!!.split(' ').map(String::toInt)
    //print("Введите координаты i и j, на которых сядет мэр: ")
    //var (nPlace, mPlace) = readLine()!!.split(' ').map(String::toInt)
    //nPlace--; mPlace--
    //var (nSize, mSize) = "10 10".split(' ').map(String::toInt)
    val (nPlace, mPlace) = "1 1".split(' ').map(String::toInt)

    println("Введите название файла с вводными данными:")
    val input = File(readLine())
    val txts = input.readLines().drop(1)
    val re = Regex("[^0-9]")

    for (i in txts) {
        val (nS, mS) = i.split(' ').map(String::toString)
        val mSize = re.replace(mS, "").toInt()
        val nSize = re.replace(nS, "").toInt()
        val arr2d = main2(nSize, mSize, nPlace, mPlace)
        for (b in arr2d) {
            for (a in b) {
                print(a.toString() + '\t')
            }
            println()
        }
        println()
    }
}