package com.example.randomfilm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var films: ArrayList<String>
    private lateinit var textView: TextView
    private lateinit var rand: Random
    private lateinit var btnReset: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        films = resources.getStringArray(R.array.films).toCollection(ArrayList())
        textView = findViewById(R.id.textView)
        btnReset = findViewById(R.id.btnReset)
        rand = Random()
    }

    fun btnFilm(view: View) {
        if (films.size == 0) {
            textView.text = "Уникальные фильмы закончились"
        } else {
            val i = rand.nextInt(films.size)
            textView.text = films[i]
            films.removeAt(i)
        }
    }

    fun btnReset(view: View) {
        textView.text = "Нажмите кнопку"
        films = resources.getStringArray(R.array.films).toCollection(ArrayList())
    }
}