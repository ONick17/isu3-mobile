data class Message(val address: String?, val topic: String?, val sender: String?, val text: String?) {
    fun toHTML(): String {
        val html_head1 = """
<!DOCKTYPE html>
<html>
<head>
<title>Kotlin-HTML</title>"""

        val html_style = """
<style type='text/css'>
table {
    margin: 10px;
    font-family: 'Lucida Sans Unicode';
    font-size: 14px;
    border-collapse: collapse;
    text-align: center;
}

td {                    
    border-style: solid;
    border-width: 1px;
    border-color: black;
    background: white;
    color: black;
    padding: 10px 15px;
    text-align: left;
}

.header {
    background: black;
    color: white;
    padding: 10px 15px;
    text-align: left;
}
</style>
"""

        val html_head2 = "</head>\n"

        val html_body = "<body>\n\t<table>\n" +
                if(address!=null) address.let{ "\t\t<tr><td class='header'>address</td><td>$address</td></tr>\n" } else {""} +
                if(topic!=null) topic.let{ "\t\t<tr><td class='header'>topic</td><td>$topic</td></tr>\n" } else {""} +
                if(sender!=null) sender.let{ "\t\t<tr><td class='header'>sender</td><td>$sender</td></tr>\n" } else {""} +
                if(text!=null) text.let{ "\t\t<tr><td class='header'>text</td><td>$text</td></tr>\n" } else {""} +
                "\t</table>\n</body>\n</html>"

        return html_head1+html_style+html_head2+html_body
    }
}

fun main() {
    /*
    fun Message.toHTML(): String {
        val template = "<table> " +
                address?.let { "<tr><td>address</td><td>$it</td></tr> \n" } +
                topic?.let { "<tr><td>address</td><td>$it</td></tr> \n" } +
                "</table>"
        return template
    }
    */
    val keke = Message("lebedev@iszf.irk.ru", null, "vlad", "helllooooooo")
    println(keke.toHTML())
}