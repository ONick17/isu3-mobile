package com.example.makelayout

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.Random
import android.view.Gravity
import android.util.Log

class MainActivity : AppCompatActivity() {
    var xSize = 4
    var ySize = 4
    var openCardsCount = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val cards = arrayListOf(
            R.drawable.c1_1,
            R.drawable.c2_1, R.drawable.c2_2, R.drawable.c2_3, R.drawable.c2_4,
            R.drawable.c3_1, R.drawable.c3_2, R.drawable.c3_3, R.drawable.c3_4,
            R.drawable.c4_1, R.drawable.c4_2, R.drawable.c4_3, R.drawable.c4_4,
            R.drawable.c5_1, R.drawable.c5_2, R.drawable.c5_3, R.drawable.c5_4,
            R.drawable.c6_1, R.drawable.c6_2, R.drawable.c6_3, R.drawable.c6_4,
            R.drawable.c7_1, R.drawable.c7_2, R.drawable.c7_3, R.drawable.c7_4,
            R.drawable.c8_1, R.drawable.c8_2, R.drawable.c8_3, R.drawable.c8_4,
            R.drawable.c9_1, R.drawable.c9_2, R.drawable.c9_3, R.drawable.c9_4,
            R.drawable.c10_1, R.drawable.c10_2, R.drawable.c10_3, R.drawable.c10_4,
            R.drawable.c11_1, R.drawable.c11_2, R.drawable.c11_3, R.drawable.c11_4,
            R.drawable.c12_1, R.drawable.c12_2, R.drawable.c12_3, R.drawable.c12_4,
            R.drawable.c13_1, R.drawable.c13_2, R.drawable.c13_3, R.drawable.c13_4,
            R.drawable.c14_1, R.drawable.c14_2, R.drawable.c14_3, R.drawable.c14_4)

        val rand = Random()
        val field = Array(ySize){Array(xSize){0}}
        var cardsCount = xSize*ySize / 2 * 2
        var randomCards = cards.shuffled().take(cardsCount / 2)
        randomCards = (randomCards + randomCards).toMutableList()
        //Log.d("random_cards", randomCards.toString())
        var firstCard: View = View(this)

        val endingText = TextView(applicationContext)
        endingText.textSize = 50f
        endingText.text = "The End"
        endingText.setGravity(Gravity.CENTER)

        val layout0 = LinearLayout(applicationContext)
        val layout1 = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        val layout2 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0)
        layout0.orientation = LinearLayout.VERTICAL
        layout1.weight = 1f
        layout1.setMargins(2, 2, 2, 2)
        layout2.weight = 1f
        layout2.setMargins(2, 2, 2, 2)

        for (i in 0 until ySize){
            val layoutLine = LinearLayout(applicationContext)
            layoutLine.orientation = LinearLayout.HORIZONTAL
            for (j in 0 until xSize){
                if ((ySize*xSize%2 == 1) && (i == ySize-1) && (j == xSize-1)) {
                    break
                }
                val card = ImageView(applicationContext)
                val randCardPos = rand.nextInt(randomCards.size)
                card.setImageResource(R.drawable.c1_1)
                field[i][j] = randomCards[randCardPos]
                card.layoutParams = layout1
                card.tag = randomCards[randCardPos].toString()
                randomCards.removeAt(randCardPos)
                card.setOnClickListener(View.OnClickListener {view ->
                    val iv: ImageView = view as ImageView
                    if (openCardsCount == 0){
                        openCardsCount++
                        firstCard = view
                        view.isClickable = false
                        iv.setImageResource(field[i][j])
                    }
                    else if (openCardsCount == 1){
                        if (view.tag.equals(firstCard.tag)){
                            /*
                            delay(1000)
                            cardsCount -= 2
                            view.visibility = View.INVISIBLE
                            firstCard.visibility = View.INVISIBLE
                            view.isClickable = false
                            firstCard.isClickable = false
                            openCardsCount = 0
                            if (cardsCount == 0){
                                layout0.addView(endingText)
                            }

                             */
                            GlobalScope.launch (Dispatchers.Main) {
                                iv.setImageResource(field[i][j])
                                delay(1000)
                                cardsCount -= 2
                                view.visibility = View.INVISIBLE
                                firstCard.visibility = View.INVISIBLE
                                view.isClickable = false
                                firstCard.isClickable = false
                                openCardsCount = 0
                                if (cardsCount == 0){
                                    layout0.addView(endingText)
                                }
                            }
                        }
                        else {
                            iv.setImageResource(field[i][j])
                            openCardsCount = 2
                            GlobalScope.launch (Dispatchers.Main) {
                                delay(1000)
                                setBackgroundWithoutDelay(view)
                                setBackgroundWithoutDelay(firstCard)
                            }
                        }
                    }
                })
                layoutLine.addView(card)
            }
            layoutLine.layoutParams = layout2
            layout0.addView(layoutLine)
        }
        setContentView(layout0)
    }

    suspend fun setBackgroundWithoutDelay(v1: View) {
        val keke: ImageView = v1 as ImageView
        keke.setImageResource(R.drawable.c1_1)
        v1.isClickable = true
        openCardsCount = 0
    }

}