import java.io.File

fun main(args: Array<String>) {
    println("Введите название файла с вводными данными:")
    var input = File(readLine())
    //var input = File("test.txt")
    println("Файл с выводом: output.txt")
    var output = File("output.txt")
    output.writeText("")
    var txts = input.readLines()
    var roles = mutableMapOf<String, MutableList<String>>()
    var str: String
    var cnt: Int = 0
    var ind: Int
    var mode: Boolean = false

    for (txt in txts) {
        if (txt == "textLines:\n" || txt == "textLines:") {
            mode = true
            continue
        } else if ("roles:" in txt) {
            continue
        }
        if (mode) {
            ind = 0
            for (i in txt) {
                ind++
                if (i == ':') {
                    break
                }
            }
            var arr = roles.getValue(txt.substring(0..ind-2))
            arr += ((++cnt).toString() + ") " + txt.substring(ind+1..txt.length-1))
            roles += txt.substring(0..ind-2) to arr
        } else {
            roles += txt.substring(0..txt.length-1) to mutableListOf<String>()
        }
    }

    for (i in roles.keys) {
        output.appendText(i+":\n")
        for (j in roles.getValue(i)){
            output.appendText(j+"\n")
        }
        output.appendText("\n")
    }
}