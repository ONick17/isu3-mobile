import kotlin.math.abs

/*
data class Person(val name: String, val age: Int)
*/
fun moveRobot(r: Robot, toX: Int, toY: Int) {
    if (r.x < toX) {
        when (r.direction) {
            Direction.UP -> r.turnRight()
            Direction.DOWN -> r.turnLeft()
            Direction.LEFT -> r.turnBack()
            Direction.RIGHT -> {}
        }
    } else {
        when (r.direction) {
            Direction.UP -> r.turnLeft()
            Direction.DOWN -> r.turnRight()
            Direction.LEFT -> {}
            Direction.RIGHT -> r.turnBack()
        }
    }
    repeat (abs(toX - r.x)) {
        r.stepForward()
    }
    if (r.y < toY) {
        when (r.direction) {
            Direction.UP -> {}
            Direction.DOWN -> r.turnBack()
            Direction.LEFT -> r.turnRight()
            Direction.RIGHT -> r.turnLeft()
        }
    } else {
        when (r.direction) {
            Direction.UP -> r.turnBack()
            Direction.DOWN -> {}
            Direction.LEFT -> r.turnLeft()
            Direction.RIGHT -> r.turnRight()
        }
    }
    repeat (abs(toY - r.y)) {
        r.stepForward()
    }
}

enum class Direction {
    UP, DOWN, LEFT, RIGHT
}

fun main() {
    //val p = Person("Vasya", 14)
    //println(p)
    val r1 = Robot(3, 4, Direction.RIGHT)
    println(r1)
    moveRobot(r1, 4, 2)
    println(r1)
    moveRobot(r1, 7, 8)
    println(r1)
    moveRobot(r1, 0, 0)
    println(r1)
}