package com.example.hw13_arrayadapter

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import java.util.Random

class MainActivity : AppCompatActivity() {
    private lateinit var names: ArrayList<String>
    private lateinit var l_names: ArrayList<String>
    private lateinit var lsNames: ArrayList<String>
    private lateinit var btnNew: Button
    private lateinit var rand: Random

    //@SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        names = resources.getStringArray(R.array.names).toCollection(ArrayList())
        l_names = resources.getStringArray(R.array.s_names).toCollection(ArrayList())
        var lsNames = findViewById<ListView>(R.id.lsNames)
        btnNew = findViewById(R.id.btnNew)
        var f_names: MutableList<String> = ArrayList()
        var adapter: ArrayAdapter<String> = ArrayAdapter(this, android.R.layout.simple_list_item_1, f_names)
        var f_name: String
        lsNames.setAdapter(adapter)
        rand = Random()
        btnNew.setOnClickListener{
            f_name = names[rand.nextInt(names.size)] + " " + l_names[rand.nextInt(l_names.size)]
            f_names.add(f_name)
            adapter.notifyDataSetChanged()
        }
    }
}
