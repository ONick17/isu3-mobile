package com.example.hw18_tiles

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.Log
//import android.widget.LinearLayout
//import android.widget.TextView
import android.widget.Toast
//import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import java.lang.Integer.min
import java.util.*


class MyView(context: Context?): View(context) {
    val rand = Random()
    val field = Array(4){BooleanArray(4){rand.nextBoolean()}}
    lateinit var tiles: Array<Array<Tile>>

    var myWidth = -1
    var myHeight = -1
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        tiles = Array(4){Array(4){Tile(0f, 0f, 0f)}}
        val size = 170
        for (i in tiles.indices){
            for (j in tiles[i].indices){
                tiles[i][j] = Tile(((1+3*i)*size/2).toFloat(), ((1+3*j)*size/2).toFloat(), (size).toFloat())
            }
        }
    }

    val paint = Paint()
    override fun onDraw(canvas: Canvas?) {
        paint.color = Color.RED
        canvas?.apply {
            myWidth = width
            myHeight = height
            drawColor(Color.WHITE)
            for (i in tiles.indices){
                for (j in tiles[i].indices){
                    if (field[i][j]){
                        paint.color = Color.parseColor("#C0C0C0")
                    }
                    else {
                        paint.color = Color.parseColor("#000000")
                    }
                    drawRect(tiles[i][j].x, tiles[i][j].y, tiles[i][j].x + tiles[i][j].size,
                        tiles[i][j].y + tiles[i][j].size, paint)
                }
            }
        }
    }

    var checkX = 0f
    var checkY = 0f
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event!!.action == MotionEvent.ACTION_DOWN){
            event.apply {
                checkX = x
                checkY = y
            }
            var clickedX = -1
            var clickedY = -1
            for (i in tiles.indices){
                for (j in tiles[i].indices) {
                    if (tiles[i][j].beginCheck(checkX, checkY)){
                        clickedY = i
                        clickedX = j
                    }
                }
            }
            if (clickedX == -1 && clickedY == -1){
                return true
            }
            for (i in field.indices){
                field[i][clickedX] = !field[i][clickedX]
            }
            for (i in field[0].indices){
                if (i == clickedX){
                    continue
                }
                field[clickedY][i] = !field[clickedY][i]
            }
            var mode1 = 0
            var mode2 = 0
            for (i in field.indices){
                for (j in field[i].indices){
                    if (field[i][j]){
                        mode1 += 1
                    }
                    else {
                        mode2 += 1
                    }
                }
            }
            if (mode1 == 0 || mode2 == 0){
                Toast.makeText(context, "The End", Toast.LENGTH_LONG).show()
                /*
                val endingText = TextView(context)
                endingText.textSize = 50f
                endingText.text = "The End"
                endingText.setGravity(Gravity.CENTER)
                val layout0 = LinearLayout(context)
                layout0.addView(endingText)
                setContentView(layout0)
                */
            }
            invalidate()
        }
        return true
    }
}


data class Tile(val x: Float, val y: Float, val size: Float) {
    fun beginCheck(pointX: Float, pointY: Float): Boolean {
        return(x <= pointX && pointX <= x + size && y <= pointY && pointY <= y + size)
    }
}


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(MyView(this))
    }
}
