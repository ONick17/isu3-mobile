import kotlin.math.max
import kotlin.math.abs

fun kira(k:Int, x:Int, y:Int) : Int{
    var rez: Int
    val max = if (x > y) x else y
    val min = if (x < y) x else y
    if (k > min+2)
        rez = k - max
    else
        rez = min+2 - max
    return(rez)
}

fun vlad1(k:Int, x:Int, y:Int) : Int{
    var rez: Int
    val max = if (x > y) x else y
    val min = if (x < y) x else y
    if (((x+1==k) && (x>=y)) || ((y+1==k) && (y>=x))){
        return(1)
    }
    if (k > min+2)
        rez = k - max
    else
        rez = min+2 - max
    return(rez)
}

fun vlad2(k:Int, x:Int, y:Int) : Int{
    var rez: Int
    val max = if (x > y) x else y
    val min = if (x < y) x else y
    if (((x+1==k) && (x>=y)) || ((y+1==k) && (y>=x))){
        return(1)
    }
    if (((x>k) && (x-y>1)) || ((y>k) && (y-x>1))) {
        return(0)
    }
    if (k > min+2)
        rez = k - max
    else
        rez = min+2 - max
    return(rez)
}

fun vlad3(k:Int, x:Int, y:Int) : Int{
    var rez: Int
    val max = if (x > y) x else y
    val min = if (x < y) x else y
    if (((x>k) && (x-y>1)) || ((y>k) && (y-x>1))) {
        return(0)
    }
    if (k > min+2)
        rez = k - max
    else
        rez = min+2 - max
    return(rez)
}

fun max(k:Int, x:Int, y:Int) : Int{
    if(abs(x - y) >= 2 && max(x, y) >= k){
        return(0)
    }
    else if(x == y){
        if(x >= k){
            return(2)
        }
        else if(k - x >= 1) {
            return(k - x)
        }
        else return(2)
    }
    else if(k > max(x, y)){
        if(x > y){
            return(k - x)
        }
        else return(k - y)
    }
    else return(1)
}

fun main(args: Array<String>) {
    var kira : Int
    var max : Int
    var vlad1 : Int
    var vlad2 : Int
    var vlad3 : Int
    for (k in 0..25) {
        for (x in 0..27) {
            for (y in 0..27) {
                //if (!(((x>k) || (y>k)) && ((y-x>2)||(x-y>2)))) {
                if (true) {
                    kira = kira(k, x, y)
                    max = max(k, x, y)
                    vlad1 = vlad1(k, x, y)
                    vlad2 = vlad2(k, x, y)
                    vlad3 = vlad3(k, x, y)
                    if (kira != max) {
                        println("max: " + '\t' + max.toString())
                        println("kira: " + '\t' + kira.toString())
                        println("vlad1: " + '\t' + vlad1.toString())
                        println("vlad2: " + '\t' + vlad2.toString())
                        println("vlad3: " + '\t' + vlad3.toString())
                        println("k, x, y: " + k.toString() + ' ' + x.toString() + ' ' + y.toString())
                        println()
                    }
                }
            }
        }
    }
}