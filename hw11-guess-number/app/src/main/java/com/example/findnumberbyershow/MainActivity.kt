package com.example.findnumberbyershow
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var startVal: EditText
    private lateinit var endVal: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startVal = findViewById(R.id.startD)
        endVal = findViewById(R.id.endD)
    }

    fun buttonContinue(view: View){
        val startNum =startVal.text.toString().toInt()
        val endNum = endVal.text.toString().toInt()

        if (startNum >= endNum){
            Toast.makeText(this, "Начало диапазона должно быть меньше конца", Toast.LENGTH_LONG).show()
        }
        else{
            val intent = Intent(this, MainActivity2::class.java)
            intent.putExtra("start", startNum)
            intent.putExtra("end", endNum)
            startActivity(intent)
        }
    }
}