package com.example.findnumberbyershow

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlin.math.min

class MainActivity2 : AppCompatActivity() {
    var startVal = 0
    var endVal = 0
    private lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        startVal = intent.getIntExtra("start", 0)
        endVal = intent.getIntExtra("end", 0)
        textView = findViewById(R.id.text_number)
        textView.text = ((startVal + endVal)/2).toString()
    }

    fun changeNum(view: View){
        if(view.id == R.id.btn_correct){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        else if(view.id == R.id.btn_lower){
            endVal = (startVal + endVal)/2
        }
        else if(view.id == R.id.btn_bigger){
            startVal = (startVal + endVal)/2
        }
        textView.text = ((startVal + endVal) / 2).toString()
    }

    fun buttonBack(view: View){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}