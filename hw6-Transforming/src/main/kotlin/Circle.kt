class Circle(var x: Int, var y: Int, var radius: Int) : Movable, Figure(1), Transforming {
    var color: Int = -1

    override val name: String
        get() = "Circle"
    constructor(circle: Circle) : this(circle.x, circle.y, circle.radius)

    override fun move(dx: Int, dy: Int) {
        x += dx; y += dy
    }
    override fun area(): Float {
        return (3.14*radius*radius).toFloat()
    }

    override fun resize(zoom: Int) {
        radius *= zoom
    }

    override fun rotate(dir: RotateDirection, centerX: Int, centerY: Int) {
        when (dir) {
            RotateDirection.CounterClockwise -> {
                y = x - centerX + centerY.also {
                    x = -(y - centerY) + centerX
                }
            }

            RotateDirection.Clockwise -> {
                y = -(x - centerX) + centerY.also {
                    x = y - centerY + centerX
                }
            }
        }
    }
}