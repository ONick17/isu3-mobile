import kotlin.math.sqrt

// сочетание определения класса и конструктора одновременно объявляет переменные и задаёт их значения
class Rect(var x: Int, var y: Int, var width: Int, var height: Int) : Movable, Figure(0), Transforming {
    var color: Int = -1 // при объявлении каждое поле нужно инициализировать


    override val name: String
        get() = "Rectangle"
    constructor(rect: Rect) : this(rect.x, rect.y, rect.width, rect.height)

    // нужно явно указывать, что вы переопределяете метод
    override fun move(dx: Int, dy: Int) {
        x += dx; y += dy
    }

    // для каждого класса area() определяется по-своему
    override fun area(): Float {
        return (width*height).toFloat() // требуется явное приведение к вещественному числу
    }

    override fun resize(zoom: Int) {
        height *= zoom
        width *= zoom
    }

    override fun rotate(dir: RotateDirection, centerX: Int, centerY: Int) {
        when (dir) {
            RotateDirection.CounterClockwise -> {
                y = x - centerX + centerY.also {
                    x = -(y - centerY) + centerX
                }
            }

            RotateDirection.Clockwise -> {
                y = -(x - centerX) + centerY.also {
                    x = y - centerY + centerX
                }
            }
        }
        width = height.also{
            height = width
        }
    }
}