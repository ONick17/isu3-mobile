class Square(var x: Int, var y: Int, var width: Int) : Movable, Figure(2), Transforming {
    var color: Int = -1

    override val name: String
        get() = "Square"
    constructor(square: Square) : this(square.x, square.y, square.width)

    // нужно явно указывать, что вы переопределяете метод
    override fun move(dx: Int, dy: Int) {
        x += dx; y += dy
    }

    // для каждого класса area() определяется по-своему
    override fun area(): Float {
        return (width*width).toFloat() // требуется явное приведение к вещественному числу
    }

    override fun resize(zoom: Int) {
        width *= zoom
    }

    override fun rotate(dir: RotateDirection, centerX: Int, centerY: Int) {
        when (dir) {
            RotateDirection.CounterClockwise -> {
                y = x - centerX + centerY.also {
                    x = -(y - centerY) + centerX
                }
            }

            RotateDirection.Clockwise -> {
                y = -(x - centerX) + centerY.also {
                    x = y - centerY + centerX
                }
            }
        }
    }
}