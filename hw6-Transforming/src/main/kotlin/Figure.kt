abstract class Figure(val id: Int) : FigureInterface {
    abstract fun area() : Float
}