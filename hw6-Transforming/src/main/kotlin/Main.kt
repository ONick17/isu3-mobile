fun main() {
    println("Rect")
    val f_r = Rect(0, 0, 4, 2)

    //перемещаю на другие кординаты
    println(f_r.x.toString() + " " + f_r.y.toString())
    f_r.move(4, 3)
    println(f_r.x.toString() + " " + f_r.y.toString())
    println()

    //увеличиваю каждую сторону в 2 раза
    println(f_r.area())
    f_r.resize(2)
    println(f_r.area())
    println()

    //поворот по часовой
    println(f_r.x.toString() + " " + f_r.y.toString())
    f_r.rotate(RotateDirection.Clockwise, 3, -3)
    println(f_r.x.toString() + " " + f_r.y.toString())
    //полный круг против часовой
    f_r.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_r.x.toString() + " " + f_r.y.toString())
    f_r.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_r.x.toString() + " " + f_r.y.toString())
    f_r.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_r.x.toString() + " " + f_r.y.toString())
    f_r.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_r.x.toString() + " " + f_r.y.toString())

    println()
    println("Circle")
    val f_c = Circle(0, 0, 1)

    //перемещаю на другие кординаты
    println(f_c.x.toString() + " " + f_c.y.toString())
    f_c.move(4, 3)
    println(f_c.x.toString() + " " + f_c.y.toString())
    println()

    //увеличиваю радиус в 2 раза
    println(f_c.area())
    f_c.resize(2)
    println(f_c.area())
    println()

    //поворот по часовой
    println(f_c.x.toString() + " " + f_c.y.toString())
    f_c.rotate(RotateDirection.Clockwise, 3, -3)
    println(f_c.x.toString() + " " + f_c.y.toString())
    //полный круг против часовой
    f_c.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_c.x.toString() + " " + f_c.y.toString())
    f_c.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_c.x.toString() + " " + f_c.y.toString())
    f_c.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_c.x.toString() + " " + f_c.y.toString())
    f_c.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_c.x.toString() + " " + f_c.y.toString())

    println()
    println("Square")
    val f_s = Square(0, 0, 1)

    //перемещаю на другие кординаты
    println(f_s.x.toString() + " " + f_s.y.toString())
    f_s.move(4, 3)
    println(f_s.x.toString() + " " + f_s.y.toString())
    println()

    //увеличиваю сторону в 2 раза
    println(f_s.area())
    f_s.resize(2)
    println(f_s.area())
    println()

    //поворот по часовой
    println(f_s.x.toString() + " " + f_s.y.toString())
    f_s.rotate(RotateDirection.Clockwise, 3, -3)
    println(f_s.x.toString() + " " + f_s.y.toString())
    //полный круг против часовой
    f_s.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_s.x.toString() + " " + f_s.y.toString())
    f_s.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_s.x.toString() + " " + f_s.y.toString())
    f_s.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_s.x.toString() + " " + f_s.y.toString())
    f_s.rotate(RotateDirection.CounterClockwise, 3, -3)
    println(f_s.x.toString() + " " + f_s.y.toString())
}