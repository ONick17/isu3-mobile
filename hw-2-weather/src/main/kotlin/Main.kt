fun main(args: Array<String>) {
    var N : Int = readLine()!!.toInt()
    var data = readLine()!!.split(' ').map{it.toFloat()}
    var dataNew = FloatArray(N)
    for (i in 0 until N) {
        if (i == 0) {
            dataNew[i] = (data[i] + data[i+1])/2
        } else if (i == N-1) {
            dataNew[i] = (data[i-1] + data[i])/2
        } else {
            dataNew[i] = (data[i-1] + data[i] + data[i+1])/3
        }
    }
    println(dataNew.contentToString())
}