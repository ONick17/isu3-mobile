import java.io.File
import kotlin.math.max
import kotlin.math.min

fun max_row(field: MutableList<MutableList<Int>>): Int {
    var max = 0
    var cnt : Int
    for (j in 0 until field.size) {
        cnt = 0
        for (i in 0 until field[0].size-1) {
            if (field[j][i] == 1) {
                cnt += 1
            } else {
                if (cnt > max) {
                    max = cnt
                }
                cnt = 0
            }
        }
        if (cnt > max) {
            max = cnt
        }
    }
    return max
}

fun max_column(field: MutableList<MutableList<Int>>): Int {
    var max = 0
    var cnt : Int
    for (j in 0 until field[0].size-1) {
        cnt = 0
        for (i in 0 until field.size) {
            if (field[i][j] == 1) {
                cnt += 1
            } else {
                if (cnt > max) {
                    max = cnt
                }
                cnt = 0
            }
        }
        if (cnt > max) {
            max = cnt
        }
    }
    return max
}

/*
fun try_fill_in() : {

}

fun fill_in(field: MutableList<MutableList<Int>>) {
    var cls = field[0][field[0].size-1]
    var curr_cl = 1
    for (i in 0 until field[0].size-1) {
        if (field[0][i] != 0) {
            field[0][i] = curr_cl
            if (curr_cl == cls) {
                curr_cl = 1
            } else {
                curr_cl += 1
            }
        } else {
            curr_cl = 1
        }
    }

    var try_check : Boolean
    for (j in 1 until field.size) {
        try_check = true
        for (i in 0 until field[0].size - 1) {
            if(try_check){

            }
            field[j][i] = curr_cl
            curr_cl += 1
            if (field[j][i] == 0) {
                try_check = true
            }
        }
    }
    for (i in field) {
        for (j in i) {
            print(j.toString() + '\t')
        }
        println()
    }
    println()
}
 */

fun main2(field: MutableList<MutableList<Int>>) {
    var max_r = max_row(field)
    var max_c = max_column(field)
    if ((field[0][field[0].size-1] < max_r) || (field[0][field[0].size-1] < max_c)) {
        println("NO: " + (field.size).toString() + ' ' + (field[0].size-1).toString() + ' ' + (field[0][field[0].size-1]).toString())
        println()
        return
    }
    println("YES: " + (field.size).toString() + ' ' + (field[0].size-1).toString() + ' ' + (field[0][field[0].size-1]).toString())
    //fill_in(field)
}

fun main() {
    /*
    val t = readLine()!!.toInt()
    var fields: MutableList<MutableList<MutableList<Int>>> = ArrayList()
    for (i in 0 until t) {
        var inp = readLine()!!.split(' ').map(String::toInt)
        fields.add(mutableListOf<MutableList<Int>>())
        for (j in 0 until inp[0]) {
            var row = readLine()!!.split(' ').map(String::toInt).toMutableList()
            fields[i].add(row)
        }
        fields[i][0].add(inp[2])
    }
     */

    println("Введите название файла с вводными данными:")
    val input = File(readLine())
    val txts = input.readLines()
    var fields: MutableList<MutableList<MutableList<Int>>> = ArrayList()
    var field: MutableList<MutableList<Int>>
    val cnt = txts[0].toInt()
    var ln_num = 0
    for (i in 0 until cnt) {
        ln_num += 1
        field = ArrayList()
        var (n, m, k) = txts[ln_num].split(' ').map(String::toInt)
        for (j in 0 until n) {
            ln_num += 1
            var inp = txts[ln_num].split(' ').map(String::toInt).toMutableList()
            field.add(inp)
        }
        field[0].add(k)
        fields.add(field)
    }

    for (field in fields) {
        main2(field)
    }
}