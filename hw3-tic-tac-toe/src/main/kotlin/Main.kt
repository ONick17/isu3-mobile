fun showAll(fields : MutableList<MutableList<MutableList<MutableList<String>>>>, curX : Int, curY : Int){
    val n = fields.size
    for (fieldsRow in 0 until n){
        for (fieldRow in 0 until n){
            for (fieldsColumn in 0 until n){
                for (i in 0 until n){
                    if ((fieldsRow==curX) && (fieldsColumn==curY)) {
                        print(fields[fieldsRow][fieldsColumn][fieldRow][i].uppercase() + '\t')
                    } else {
                        print(fields[fieldsRow][fieldsColumn][fieldRow][i] + '\t')
                    }
                }
                print('\t')
            }
            println()
        }
        println()
    }
}

fun showOne(field : MutableList<MutableList<String>>){
    val n = field.size
    for (j in 0 until n){
        for (i in 0 until n){
            print(field[j][i] + '\t')
        }
        println()
    }
}

fun createFields(sz : Int) : MutableList<MutableList<MutableList<MutableList<String>>>> {
    var fields : MutableList<MutableList<MutableList<MutableList<String>>>> = mutableListOf()
    var fieldsRow : MutableList<MutableList<MutableList<String>>>
    var field : MutableList<MutableList<String>>
    var row : MutableList<String>
    for (a in 0 until sz){
        fieldsRow = mutableListOf()
        for (b in 0 until sz){
            field = mutableListOf()
            for (c in 0 until sz){
                row = mutableListOf()
                for (d in 0 until sz) {
                    row.add("o")
                }
                field.add(row)
            }
            fieldsRow.add(field)
        }
        fields.add(fieldsRow)
    }
    return(fields)
}

fun printHelp() {
    println("Список команд:")
    println("help - вывод списка команд")
    println("fields - вывод всех игровых полей")
    println("field - вывод действующего игрового поля")
    println("exit - сдаться и закончить игру")
    println()
}

fun turn() : Int {
    val mode = readLine()!!
    val re = Regex("[^0-9]")
    if(mode=="help"){
        printHelp()
        return(0)
    }
    if(mode=="fields"){
        return(1)
    }
    if(mode=="field"){
        return(2)
    }
    if(mode=="exit"){
        return(3)
    }
    try{
        val a = mode.split(' ').map(String::toString)
        if (a.size > 2) {
            println("Вы нарушили ввод! Попробуйте вновь.")
            return(0)
        }
        var modeAx = re.replace(a[0], "").toInt()
        var modeAy = re.replace(a[1], "").toInt()
        return(modeAx*100+modeAy)
    } catch (e: NumberFormatException) {
        println("Вы нарушили ввод! Попробуйте вновь.")
        return(0)
    } catch (e: IndexOutOfBoundsException) {
        println("Вы нарушили ввод! Попробуйте вновь.")
        return(0)
    }
}

fun winCheck(field : MutableList<MutableList<String>>, player : Int) : Boolean {
    val n = field.size
    var cnt1 : Int
    var cnt2 : Int
    val keke = if (player == 0) "a" else "b"
    var toWin = 3
    //проверка горизонталей и вертикалей
    for (i in 0 until n) {
        cnt1 = 0
        cnt2 = 0
        for (j in 0 until n) {
            if (field[i][j] == keke) {cnt1++} else {cnt1 = 0}
            if (field[j][i] == keke) {cnt2++} else {cnt2 = 0}
            if ((cnt1 == toWin) || (cnt2 == toWin)) {
                return(true)
            }
        }
    }
    //проверка диагоналей слева направо
    var i1 : Int
    var i2 : Int
    var j12 : Int
    for (i in 0 until n) {
        for (j in 0 until n) {
            cnt1 = 0
            cnt2 = 0
            for (zhopa in 0 until n-j) {
                i1 = i+zhopa
                i2 = i-zhopa
                j12 = j+zhopa
                //сверху вниз
                if ((i1 >= n) || (j12 >= n)) {continue}
                if (field[i1][j12] == keke) {cnt1++} else {cnt1 = 0}
                if (cnt1 == toWin) {return(true)}
                //снизу вверх
                if (i2 < 0) {continue}
                if (field[i2][j12] == keke) {cnt2++} else {cnt2 = 0}
                if (cnt2 == toWin) {return(true)}
            }
        }
    }
    return(false)
}

fun main(args: Array<String>) {
    var sz = 5
    var fields = createFields(sz)
    var curField = mutableListOf(sz/2, sz/2)
    var curPlayer = 0
    var win = 2
    var turn : Int
    var x : Int
    var y : Int
    val re = Regex("[^0-9]")
    println("help - список команд")

    while (true){
        println("Поле " + (curField[0]+1).toString()+'.'+(curField[1]+1).toString()+':')
        showOne(fields[curField[0]][curField[1]])
        while (true){
            if (curPlayer==0) {
                print("Ход игрока A: ")
            } else {
                print("Ход игрока B: ")
            }
            turn = turn()
            if (turn == 1) {
                showAll(fields, curField[0], curField[1])
                continue
            }
            if (turn == 2) {
                println("Поле " + (curField[0]+1).toString()+'.'+(curField[1]+1).toString()+':')
                showOne(fields[curField[0]][curField[1]])
                continue
            }
            if (turn == 3) {
                win = 1-curPlayer
                break
            }
            if (turn > 100) {
                x = turn/100 - 1
                y = turn%100 - 1
                if (((x<0)||(y<0)) || ((x>=sz)||(y>=sz))) {
                    print("Вы ввели неверные координаты! Попробуйте вновь.")
                    continue
                }
                if (fields[curField[0]][curField[1]][x][y] != "o") {
                    print("Ячейка занята! Попробуйте вновь.")
                    continue
                }
                if (curPlayer==0) {
                    fields[curField[0]][curField[1]][x][y] = "a"
                } else {
                    fields[curField[0]][curField[1]][x][y] = "b"
                }
                if (winCheck(fields[curField[0]][curField[1]], curPlayer)) {
                    win = curPlayer
                }
                curField = mutableListOf(x, y)
                break
            }
        }
        if (win == 1){
            println("Игрок B победил!")
            break
        } else if (win == 0){
            println("Игрок A победил!")
            break
        }
        curPlayer = (curPlayer+1) % 2
    }
}