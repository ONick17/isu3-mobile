import java.util.*

const val k = 1

fun test(k: Int, j: Long): Int {
    fun one() = 1
    return one()
}

fun square(i: Int): Long = (i*i).toLong()

fun table(row: Int, col: Int) : Int{

    return 0
}

fun main(args: Array<String>) {
    println(test(1, j=2))
    val a = arrayOf(1, 2, 3)
    a[1] = 4
    println(a.contentToString())
    println()
    val sc = Scanner(System.`in`)
    val arr2d = Array(3) {row -> Array(3){col -> row*3 + col} }
    for (row in arr2d){
        println(row.contentToString())
    }
    println()
    var arrMul = Array(9) {a -> Array(9){b -> (a+1)*(b+1)} }
    for (row in arrMul){
        println(row.contentToString())
    }
}